from django.contrib import admin

from .models import Organization_info, Tags, Mapping

# Register your models here.


class Organization(admin.ModelAdmin):
    search_fields = ('org_name', 'org_site')
    list_display = ('org_name', 'org_site', 'date_added', 'date_modified')

admin.site.register(Organization_info, Organization)

class Tagss(admin.ModelAdmin):
    '''search_fields = ('tags')
    list_display = ('tags')'''

admin.site.register(Tags, Tagss)

class OrgMapping(admin.ModelAdmin):
    '''search_fields = ('tags')
    list_display = ('tags')'''

admin.site.register(Mapping, OrgMapping)


# Register your models here.

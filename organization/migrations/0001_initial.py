# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-17 18:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Organization_info',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('org_name', models.CharField(blank=True, max_length=100, verbose_name='Organization')),
                ('org_description', models.TextField()),
                ('org_icon', models.ImageField(null=True, upload_to='org_icon')),
                ('org_site', models.CharField(blank=True, max_length=200, null=True, verbose_name='Site')),
                ('org_google_plus', models.CharField(blank=True, max_length=200, null=True, verbose_name='Google Plus')),
                ('org_facebook', models.CharField(blank=True, max_length=200, null=True, verbose_name='Facebook')),
                ('org_twitter', models.CharField(blank=True, max_length=200, null=True, verbose_name='Twiter')),
                ('org_blogger', models.CharField(blank=True, max_length=200, null=True, verbose_name='Blogger')),
                ('org_email', models.CharField(blank=True, max_length=200, null=True, verbose_name='Email')),
                ('org_irc', models.CharField(blank=True, max_length=200, null=True, verbose_name='IRC')),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tags',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tags', models.CharField(blank=True, max_length=64, verbose_name='Organization')),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='organization.Organization_info')),
            ],
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0002_auto_20160216_1244'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tags',
            old_name='tags',
            new_name='tag',
        ),
    ]

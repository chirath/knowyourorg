from __future__ import unicode_literals
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator, MaxValueValidator, MinValueValidator
from mail_templated import send_mail
from django.db import models


# Create your models here.

class Organization_info(models.Model):

    org_name = models.CharField(_('Organization'), max_length=100, blank=True)
    org_description = models.TextField()

    org_icon = models.ImageField(upload_to='org_icon', null = True)

    org_site =  models.CharField(_('Site'), max_length=200, blank=True, null=True)
    org_google_plus = models.CharField(_('Google Plus'), max_length=200, blank=True, null=True)
    org_facebook = models.CharField(_('Facebook'), max_length=200, blank=True, null=True )
    org_twitter = models.CharField(_('Twiter'), max_length=200, blank=True, null=True )
    org_blogger = models.CharField(_('Blogger'), max_length=200, blank=True, null=True )
    org_email = models.CharField(_('Email'), max_length=200, blank=True, null=True )
    org_irc = models.CharField(_('IRC'), max_length=200, blank=True, null=True )

    date_added = models.DateTimeField(auto_now_add=True, auto_now=False)
    date_modified = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __str__(self):
        return self.org_name

    def __unicode__(self):
        return self.org_name


class Tags(models.Model):
    tag = models.CharField(_('tag'), max_length=64, blank=True)
    organization = models.ForeignKey('Organization_info')

    def __str__(self):
        return self.tag

    def __unicode__(self):
        return self.tag


class Mapping(models.Model):
    organization = models.ForeignKey(Organization_info)
    user = models.ForeignKey('registration.User')

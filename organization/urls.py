from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from organization import views
from organization.views import *
from organization.models import *
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^organization/org/$', orgpage.as_view(), name='org'),
]
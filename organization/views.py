from django.shortcuts import render
from organization.models import *
from django.shortcuts import redirect
from thinkfoss import settings
from django.views.generic.edit import FormView, UpdateView
from django.core.urlresolvers import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView
from braces.views import LoginRequiredMixin, AnonymousRequiredMixin
from registration.models import *
from registration.forms import *
from registration.models import *
from django.views.generic import ListView
from django.http import Http404
from django.http import HttpResponse
import simplejson as json
from django.views.generic import TemplateView
from django.shortcuts import render_to_response
from django.views.generic.edit import FormView, UpdateView
from django.core.urlresolvers import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView
from braces.views import LoginRequiredMixin, AnonymousRequiredMixin
from django.core.mail import EmailMessage
from mail_templated import send_mail
import uuid
from string import Template
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

class CurrentUserMixin(object):
    model = User

    def get_object(self, *args, **kwargs):
        try: obj = super(CurrentUserMixin, self).get_object(*args, **kwargs)
        except AttributeError: obj = self.request.user
        return obj


# Create your views here
def anonymous_required(func):
    def as_view(request, *args, **kwargs):
        redirect_to = kwargs.get('next', settings.LOGIN_REDIRECT_URL )
        if request.user.is_authenticated():
            return redirect(redirect_to)
        response = func(request, *args, **kwargs)
        return response
    return as_view


class OrgPageView(LoginRequiredMixin, ListView):
    template_name = "org.html"

    def get_queryset(self):
        org = Organization_info.objects.all()
        return org


from django.contrib import admin
from registration.models import User

class UserAdmin(admin.ModelAdmin):
    search_fields = ('usename', 'email')
    list_display = ('username', 'email', 'date_joined')
admin.site.register(User, UserAdmin)



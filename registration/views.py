from django.shortcuts import redirect
from thinkfoss import settings
from django.views.generic.edit import FormView, UpdateView
from django.core.urlresolvers import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView
from braces.views import LoginRequiredMixin, AnonymousRequiredMixin
from registration.models import *
from registration.forms import *
from django.views.generic import ListView
from django.http import Http404
from django.http import HttpResponse
import simplejson as json
from django.views.generic import TemplateView
from django.shortcuts import render_to_response
from django.core.mail import EmailMessage
from mail_templated import send_mail
import uuid
from ccavutil import encrypt,decrypt
from string import Template
from django.views.decorators.csrf import csrf_exempt

class CurrentUserMixin(object):
    model = User

    def get_object(self, *args, **kwargs):
        try: obj = super(CurrentUserMixin, self).get_object(*args, **kwargs)
        except AttributeError: obj = self.request.user
        return obj


# Create your views here
def anonymous_required(func):
    def as_view(request, *args, **kwargs):
        redirect_to = kwargs.get('next', settings.LOGIN_REDIRECT_URL )
        if request.user.is_authenticated():
            return redirect(redirect_to)
        response = func(request, *args, **kwargs)
        return response
    return as_view




class HomePageView(TemplateView):
    template_name = "index.html"


class UserProfileView(LoginRequiredMixin, CurrentUserMixin, DetailView):
    pass


class UserRegistrationView(AnonymousRequiredMixin, FormView):
    template_name = "login.html"
    authenticated_redirect_url = reverse_lazy(u"home")
    form_class = UserRegistrationForm
    success_url = '/register/user/success/'

    def form_valid(self, form):
        form.save()
        print "hello"
        return FormView.form_valid(self, form)
    
    
'''class MentorRegistrationView(LoginRequiredMixin, CurrentUserMixin, UpdateView):
    template_name = "register_mentor.html"
    authenticated_redirect_url = reverse_lazy(u"home")
    form_class = MentorRegistrationForm
    success_url = '/register/user/success/'

    def form_valid(self, form):
        form.save()
        print "hello"
        return FormView.form_valid(self, form)'''

class MentorRegistrationView(LoginRequiredMixin, CurrentUserMixin, UpdateView):
    model = User
    fields = mentor_fields
    widgets = user_extra_widgets
    template_name = "register_mentor.html"
    success_url = '/register/user/profile/edit/success/'


class ProfileHomeView(LoginRequiredMixin,TemplateView):
    template_name = 'portal/portal.html'

    def get_context_data(self, **kwargs):
        user = self.request.user
        context = super(ProfileHomeView, self).get_context_data(**kwargs)
        return context



class UserProfileCommonView(DetailView):
    template_name = 'registration/user_detail.html'

    def get_object(self, queryset=None):
            user_id = self.kwargs['user_id']
            obj = User.objects.get(user_id=self.kwargs['user_id'])
            if obj:
                return obj
            else:
                raise Http404("No details Found.")



class UserProfileUpdateView(LoginRequiredMixin, CurrentUserMixin, UpdateView):
    model = User
    fields = mentor_fields
    widgets = user_extra_widgets
    template_name = "edit_profile.html"
    success_url = '/register/user/profile/edit/success/'

'''class UserProfileUpdateView1(LoginRequiredMixin, CurrentUserMixin, UpdateView):
    model = User
    fields = user_fields
    widgets = user_widgets
    template_name = "edit_profile.html"
    success_url = '/register/user/profile/edit/success/' '''
